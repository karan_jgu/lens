<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
    public $baseModel = "";
    protected $modelInstance;

    protected $httpErrors = [
        "not_found" => "Not found",
        "unknown" => "Unknown Error",
        "relationship" => "Incorrect Relationship Type",
        "no_file" => "No file uploaded",
        "invalid_file" => "Invalid file uploaded",
        "image_save" => "Unable to save image"
    ];

    public function __construct()
    {
        $this->modelInstance = new $this->baseModel;
    }

    public function find($id){
        return $this->baseModel::find($id);
    }

    public function success($data){
        return response()->json($data,200);
    }

    public function error(string $message,int $code){
        return response()->json(['errorMessage' => $message],$code);
    }
}
