<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 12:58 PM
 */

namespace App\Http\Controllers;

use App\Http\Helpers\Utils;
use App\Http\Transformers\LensAuthResultTransformer;
use App\Models\LensAuthRequest;
use App\Models\LensAuthResult;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LensAuthRequestController extends Controller
{

    public $baseModel = "App\Models\LensAuthRequest";

    public function create(Request $request,string $requestType){
        try {
            $authRequestType = (new LensRequestTypeController())->find($requestType);
            if ($authRequestType!=null) {
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $image = $request->file('image');
                        $fileName = Utils::generateFileName($image->getClientOriginalExtension());
                        $image->move(env('LOCAL_STORAGE'), $fileName);
                        $authRequest = new LensAuthRequest;
                        $authRequest->request_type = $requestType;
                        $authRequest->image_url = env('LOCAL_STORAGE') . $fileName;
                        if ($authRequest->save()) {
                            $results = $this->saveResults(
                                $authRequest,
                                $this->executePythonAuth($authRequest,$authRequestType->model_path)
                            );
                            return $this->success(
                                (new LensAuthResultTransformer())->transformSavedResultsForApiResponse($results)
                            );
                        } else {
                            return $this->error($this->httpErrors['image_save'], 503);
                        }
                    } else {
                        return $this->error($this->httpErrors['invalid_file'], 400);
                    }
                } else {
                    return $this->error($this->httpErrors['no_file'], 400);
                }
            } else {
                return $this->error($this->httpErrors['relationship'] . ": " . $requestType, 404);
            }
        } catch (\Exception $e) {
            return $this->error($e->getMessage(),500);
        }
    }

    private function executePythonAuth(LensAuthRequest $request, string $argModelName){
        $pythonAuthScriptPath = env('PYTHON_AUTH');
        $argImagePath = $request->image_url;
        $command = escapeshellcmd('python ' . $pythonAuthScriptPath) .
            ' ' . escapeshellarg($argImagePath) .
            ' ' . escapeshellarg($argModelName);
        $out = shell_exec($command);
        $out = str_replace("\\n", "", $out);
        $this->markRequestAsComplete($request);
        return json_decode($out);
    }

    public function markRequestAsComplete(LensAuthRequest $request){
        $request->completed_at = Carbon::now();
        return $request->save();
    }

    private function saveResults(LensAuthRequest $request,$results){
        return $request->results()->saveMany($this->transformPythonOutput($results,$request->id));
    }

    private function transformPythonOutput($output, $requestId){
        $ret = [];
        $userController = new UserController();
        foreach ($output as $faces){
            foreach ($faces as $obj){
                $obj->face_hash = $requestId . "-" . implode("", $obj->face_rect);
                $obj->face_rect = json_encode($obj->face_rect);
                if (!$obj->jgu_id){
                    $obj->jgu_id = null;
                    $ret[] = new LensAuthResult((array) $obj);
                }
                else if ($userController->userWithJguIdExists($obj->jgu_id))
                    $ret[] = new LensAuthResult((array) $obj);
            }
        }
        return $ret;
    }

}