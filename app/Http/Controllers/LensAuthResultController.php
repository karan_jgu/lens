<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 24/01/19
 * Time: 4:15 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class LensAuthResultController extends Controller
{
    public $baseModel = "App\Models\LensAuthResult";

    public function confirmResult(Request $request, int $id){
        $result = $this->find($id);
        if ($result){
            $result->confirmed = 1;
            $result->save();
            return $this->success($result);
        }else{
            return $this->error($this->httpErrors['not_found'],404);
        }
    }
}