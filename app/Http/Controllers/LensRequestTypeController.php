<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 1:02 PM
 */

namespace App\Http\Controllers;


use App\Models\LensRequestType;

class LensRequestTypeController extends Controller
{

    public $baseModel = "App\Models\LensRequestType";

    public function exists(string $id) : bool {
        return $this->find($id) == null ? false : true;
    }

}