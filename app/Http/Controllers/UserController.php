<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 28/03/19
 * Time: 1:56 PM
 */

namespace App\Http\Controllers;


use App\Models\User;

class UserController extends Controller
{

    public $baseModel = "App\Models\User";

    public function userWithJguIdExists($jguId){
        return User::where('jgu_id',$jguId)->first() ? true : false;
    }
}