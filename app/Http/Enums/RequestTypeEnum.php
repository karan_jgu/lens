<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 12:44 PM
 */

namespace App\Http\Enums;


class RequestTypeEnum
{
    const authStudent = "student";
    const authEmployee = "employee";
}