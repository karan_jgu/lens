<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 1:48 PM
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\App;

class ImageUploadHelper
{

    public static $BUCKET_NAME = "jgu-eta-lens";

    private function getS3Client(){
        return App::make('aws')->createClient('s3');
    }

    public function uploadFile(string $pathToFile){
        $this->getS3Client()->putObject(array(
            'Bucket'     => self::$BUCKET_NAME,
            'Key'        => 'YOUR_OBJECT_KEY',
            'SourceFile' => $pathToFile,
        ));
    }

}