<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 4:14 PM
 */

namespace App\Http\Helpers;


use Carbon\Carbon;

class Utils
{

    public static function generateFileName(string $extension){
        return "" . Carbon::now()->timestamp . "." . $extension;
    }

    public static function convertArrayOfObjectsToArrayOfArrays($arr,$model){
        $ret = [];
        foreach ($arr as $obj){
            $ret[] = new $model ((array) $obj);
        }
        return $ret;
    }
}