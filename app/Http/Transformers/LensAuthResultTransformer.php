<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 24/01/19
 * Time: 4:01 PM
 */

namespace App\Http\Transformers;


use App\Models\LensAuthResult;

class LensAuthResultTransformer
{

    public function transformFromPythonOutput($output,$requestId){
        $ret = [];
        foreach ($output as $faces){
            foreach ($faces as $obj){
                $obj->face_hash = $requestId . "-" . implode("", $obj->face_rect);
                $obj->face_rect = json_encode($obj->face_rect);
                if (!$obj->jgu_id)
                    $obj->jgu_id = null;
                $ret[] = new LensAuthResult((array) $obj);
            }
        }
        return $ret;
    }

    public function transformSavedResultsForApiResponse(array $results){
        $faces = [];
        $faceExists = [];
        foreach ($results as $result){
            $faceIndex = array_search($result->face_hash,$faceExists);
            if ($faceIndex!==false){
                $result->user = $result->getRelatedUser();
                $faces[$faceIndex]->predictions[] = $result;
            }else{
                $face = new \stdClass();
                $face->face_hash = $result->face_hash;
                $face->face_rect = $result->face_rect;
                $face->predictions = [];
                $result->user = $result->getRelatedUser();
                $face->predictions[] = $result;
                $faces[] = $face;
                $faceExists[] = $result->face_hash;
            }
        }
        $response = new \stdClass();
        $response->faces = $faces;
        return $response;
    }
}