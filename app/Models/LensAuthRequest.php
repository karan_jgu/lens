<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 12:51 PM
 */

namespace App\Models;


class LensAuthRequest extends BaseModel
{

    public function requestType(){
        return $this->belongsTo('App\Models\LensRequestType','request_type','request_type_id');
    }

    public function results(){
        return $this->hasMany('App\Models\LensAuthResult');
    }
    
}