<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 12:52 PM
 */

namespace App\Models;


class LensAuthResult extends BaseModel
{
    protected $guarded = ["lens_auth_request_id"];

    public function request(){
        return $this->belongsTo("App\Models\LensAuthRequest");
    }

    public function user(){
        return $this->hasOne("App\Models\User","jgu_id","jgu_id");
    }

    public function getRelatedUser(){
        return User::select('jgu_id','name','official_image')->where("jgu_id",$this->jgu_id)->first();
    }
}