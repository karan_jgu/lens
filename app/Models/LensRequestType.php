<?php
/**
 * Created by PhpStorm.
 * User: 316karan
 * Date: 07/01/19
 * Time: 12:48 PM
 */

namespace App\Models;

class LensRequestType extends BaseModel
{
    protected $table = "lens_request_types";
    protected $primaryKey = "request_type_id";
    public $incrementing = "false";

    public function authRequests(){
        return $this->hasMany('App\Models\LensAuthRequest','request_type','request_type_id');
    }
}