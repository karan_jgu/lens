<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lens_auth_results', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('lens_auth_request_id');
            $table->foreign('lens_auth_request_id')->references('id')->on('lens_auth_requests')->onDelete('cascade');

            $table->unsignedInteger('jgu_id')->nullable();
            $table->foreign('jgu_id')->references('jgu_id')->on('users')->onDelete('cascade');

            $table->float('e_distance',6,2)->nullable();
            $table->float('confidence_level',3,2)->nullable();

            $table->text('face_rect')->nullable();

            $table->unsignedSmallInteger('confirmed')->default(0);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lens_auth_results');
    }
}
